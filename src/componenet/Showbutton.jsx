import React, { Component } from "react";

export default class Showbutton extends Component {
  render() {
    return (
      <div className=" text-center pt-3">
        <button
          onClick={this.props.button}
          className=" text-white  hover:cursor-pointer bg-zinc-700 w-36 h-10 rounded"
        >
          Show StopWatch
        </button>
      </div>
    );
  }
}
