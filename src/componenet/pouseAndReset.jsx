import React, { Component } from "react";

export default class PouseAndReset extends Component {
  render() {
    return (
      <div>
        <button
          onClick={this.props.resume}
          className=" mr-3 bg-slate-500 w-20 h-10 rounded shadow-lg shadow-slate-400/50 hover:bg-green-600"
        >
          Resume
        </button>
        <button
          onClick={this.props.reset}
          className=" bg-slate-500 w-20 h-10 rounded shadow-lg shadow-slate-400/50 hover:bg-red-600"
        >
          Reset
        </button>
      </div>
    );
  }
}
