import React from "react";

function Header() {
  return (
    <h2 className=" h-32 text-6xl text-white font-normal text-center pt-14">
      🚀 Timers 🚀
    </h2>
  );
}

export default Header;
