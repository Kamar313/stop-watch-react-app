import React, { Component } from "react";

export default class DisplyTimer extends Component {
  render() {
    return (
      <div className=" mt-10 mb-10 text-6xl">
        <span>
          {this.props.state.timer.hourse >= 10
            ? this.props.state.timer.hourse
            : "0" + this.props.state.timer.hourse}
        </span>
        :
        <span>
          {this.props.state.timer.min >= 10
            ? this.props.state.timer.min
            : "0" + this.props.state.timer.min}
        </span>
        :
        <span>
          {this.props.state.timer.sec >= 10
            ? this.props.state.timer.sec
            : "0" + this.props.state.timer.sec}
        </span>
        :
        <span>
          {this.props.state.timer.sSec >= 10
            ? this.props.state.timer.sSec
            : "0" + this.props.state.timer.sSec}
        </span>
      </div>
    );
  }
}
