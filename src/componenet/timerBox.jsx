import React, { Component } from "react";
import PouseAndReset from "./pouseAndReset";
import DisplyTimer from "./DisplyTimer";

export default class TimerBox extends Component {
  componentWillUnmount() {
    this.props.cleartime();
    console.log("yes");
  }
  render() {
    return (
      <div className=" flex justify-center">
        <div className=" text-white border-4 border-emerald-100 h-80 w-96 text-center relative">
          <div
            className=" absolute right-0 hover:cursor-pointer"
            onClick={this.props.button}
          >
            <img
              src="./icons8-close-window-24.webp"
              alt="close"
              className=" bg-white hover:bg-red-600"
            />
          </div>
          <h2 className=" text-4xl font-bold text-center pb-3">Stopwatch</h2>
          <DisplyTimer state={this.props.state} />
          {this.props.state.count == 0 ? (
            <button
              onClick={this.props.statrtOrStopbtn}
              className=" bg-slate-500 w-20 h-10 rounded shadow-lg shadow-slate-400/50 hover:bg-green-600"
            >
              Start
            </button>
          ) : this.props.state.count == 1 ? (
            <button
              onClick={this.props.statrtOrStopbtn}
              className=" bg-slate-500 w-20 h-10 rounded shadow-lg shadow-slate-400/50 hover:bg-red-600"
            >
              Stop
            </button>
          ) : this.props.state.count == 2 ? (
            <PouseAndReset
              reset={this.props.reset}
              resume={this.props.resume}
            />
          ) : (
            ""
          )}
        </div>
      </div>
    );
  }
}
