import React, { Component } from "react";
import Header from "./componenet/header";
import Showbutton from "./componenet/Showbutton";
import TimerBox from "./componenet/timerBox";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      buton: true,
      count: 0,
      timer: {
        hourse: 0,
        min: 0,
        sec: 0,
        sSec: 0,
      },
    };
    this.timeInervalClaer = null;
  }
  handleClick = () => {
    this.setState({ buton: !this.state.buton });
  };
  statrtOrStopbtn = () => {
    if (this.state.count == 0) {
      this.setState({ count: this.state.count + 1 });
      this.interval();
    }
    if (this.state.count == 1) {
      this.setState({ count: this.state.count + 1 });
      clearInterval(this.timeInervalClaer);
    }
  };
  interval = () => {
    this.timeInervalClaer = setInterval(this.run, 1);
  };
  cleartime = () => {
    this.setState({ sec: (this.state.timer.sec = 0) });
    this.setState({ hourse: (this.state.timer.hourse = 0) });
    this.setState({ hourse: (this.state.timer.min = 0) });
    this.setState({ hourse: (this.state.timer.sSec = 0) });
    clearInterval(this.timeInervalClaer);
  };
  reset = () => {
    this.setState({ count: 0 });
    this.cleartime();
  };
  resume = () => {
    this.setState({ count: 1 });
    this.interval();
  };

  run = () => {
    this.setState({ sSec: this.state.timer.sSec++ });
    if (this.state.timer.sSec == 100) {
      this.setState({ sec: this.state.timer.sec++ });
      this.setState({ sSec: (this.state.timer.sSec = 0) });
    }
    if (this.state.timer.sec == 60) {
      this.setState({ sec: this.state.timer.min++ });
      this.setState({ sSec: (this.state.timer.sec = 0) });
    }
    if (this.state.timer.min == 60) {
      this.setState({ sec: this.state.timer.hourse++ });
      this.setState({ sSec: (this.state.timer.min = 0) });
    }
  };

  render() {
    return (
      <>
        <Header />
        {this.state.buton ? (
          <Showbutton button={this.handleClick} />
        ) : (
          <TimerBox
            button={this.handleClick}
            statrtOrStopbtn={this.statrtOrStopbtn}
            reset={this.reset}
            resume={this.resume}
            state={this.state}
            interval={this.interval}
            cleartime={this.cleartime}
          />
        )}
      </>
    );
  }
}
